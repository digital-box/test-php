# PHP Test

> The objective of this task is to create a small application which scrapes
  or crawls a given web page to obtain information therein.

##### Your application must retrieve

 - A value deemed to be the title of the page.
 - A short (<200 characters) description or summary of the page content.
 - A cover picture; the predominant image on the page, typically the largest.
 - A list of URLs to images found on the page.
 - The URL to the favicon, if it exists.

##### Your application must

 - Perform the above tasks on any URL provided.
 - Store the results in a database schema of your choosing.

##### During the completion of this task you must

 - Utilise Git to track your changes and version control your code

##### Sample URLs

_You can obviously use others if you'd like, but just to save time
looking for sites to crawl here are some examples._

```php

$urls = [
  'http://www.bbc.co.uk',
  'http://www.buzzfeed.com',
  'http://www.entertainmentdaily.co.uk',
  'http://www.imdb.com',
  'https://news.ycombinator.com', // intentionally one with no images
  'http://www.ebay.co.uk'
];

```

##### Bonus

_We're aware that these things will add a variable amount of time to the completion
of this task and as such these aren't requirements but some useful things to consider_

 - Ignore the images which are adverts.
 - Implement means by which to crawl sites which have specific locations of
   certain resources. For example, YouTube allows access to thumnails/stills
   of the videos by ID using a different URL. For example:
    - `http://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg`
 - Implement unit and or functional testing. You may use any framework/packages
   with which you are familiar.
 - As results re being stored in a database, perhaps you could utilise this
   as a cache to prevent the need to rescrape.


##### Notes

 - A relational database must be used for this task. If completing this task
   in our offices, you will find either MySQL server or MariaDB server installed
   on the machine. The details for which are as follows:
    - username: test
    - password: test
    - hostname: localhost / 127.0.0.1
    - port: 3306
 - You may use the internet and any manuals you require during the completion of this task.
 - You may use composer to utilise packages which may be required for this task.
 - You may use a framework if you feel it is necessary.
 - There is __no user interface required__ for this task. Simple `print_r` or `var_dump`
   of results is sufficient. Similarly, URL input may come from a hard coded
   variable, be it an array or a string etc, or `$_GET`. You are not required to
   nor expected to create user interface for the submission of URLs.
 - You may use any version of PHP between 5.3 and 5.5. 